---
title: 'Home'
layout: default
---

<div class="text-center mb-5">
    <img src="../assets/images/precursors_logo_redux.png" class="d-inline-block align-top" alt="Precursors" width="100%" style="max-width: 800px" />
</div>

# RFI: Precursors Documentation

This site documents the Precursors MMO. It is both a bible for implementation, but also here to foster a discussion and comment on areas that could be improved, what works, and what doesnt. Everything here should be considered an open exchange of ideas; there's not "secret sauce" we're trying to keep to ourselves.

## Game Design

* [Design](/game/)

## Development

* [Client Architecture](/client/)
* [Client Design](/client/design.html)
* [Server Architecture](/server/)
* [Server Design](/server/design.html)
* [Project Issues](https://gitlab.com/groups/skewed-aspect/games/precursors/-/issues)
    * _Note: You may want to check the base group's [issues](https://gitlab.com/groups/skewed-aspect/-/issues)._

### Message Queues

* [Protocol](/server/message-queue/protocol.html)
