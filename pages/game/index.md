---
title: 'Game Design'
layout: default,
sidebar: true
---

# Game Design

All the architecture in the world means nothing if you don't have a solid idea of what kind of game you're making. The architecture should be informed by the game design, and vice versa (to a more limited extent). So, what kind of game are we trying to make? Well...

> RFI: Precursors is a space MMORPG that focuses on letting players tell their story. Individuals and small groups of players explore a vibrant galaxy full of mystery, intrigue and numerous large scale players all vying for power. The player takes the role of a small time operator taking command of their first ship, and looking to make their mark on the galaxy. Which path they take, and what story they tell, will be unique to each player.

That piece of copy, while useful, isn't exactly a design. It's more a mission statement. To get to an actual design, we need to define the single most important thing in any game:

## The Core Gameplay Loop

> The player uses their ship to complete missions to earn credits to upgrade their ship to do better missions to upgrade their ship more to do better missions...

This is the single most important design factor. The game will be about the player doing missions (from NPCs or a job board) to earn money to keep upgrading their ship and progressing to better missions, and better upgrades.

### Supporting Design

What follows are several decisions that are intended to support the core gameplay loop. These should enhance the loop, making it more interesting and rewarding.

#### The Ship as Character

> The player will pick a ship at character creation, and keep that ship through the game; it is, in essence, their character.

Unlike other space games; the idea here is that the basic frame of the ship will be picked at character creation, and stay the same. The player will upgrade, modify and "build" the ship they want over time, but the basic stats, design and core play style will not change as the player progresses.

That isn't to say that the player can't start with an agile ship and make it play more like a tank; rather what's being said is that they can't take an agile ship and turn it into _the best tank in the game_. **All ships will start oriented to a specific play style, and the player will be able to tweak/customize that play style in terms of how they upgrade their ship, but the customization will only go so far.**

##### Leveling

> There is no number level for a ship, rather, the ship's "level" is proportional to the net wealth of the character.

When it comes to calculating difficulty, we will need to basically do some sort of weighting by estimated DPS, armor, etc, etc. But for rough calculations, the cost of all items equipped should be in the same ballpark for two players "at the same level".

##### Ship Outfitting

> The player will outfit their ship with armor, weapons, etc, instead of their character.

It should go without saying that since the ship is the character in this scenario, equipment is ship oriented, not player oriented. (That isn't to say characters might not have a slot or two for equipment, but there's better ways to use that design space, I feel.)

Ship equipment should work much like other games. There need to be "low level" good across all stats gear, and then higher level gear that's oriented towards a particular play style. This "higher level" gear will be how players start differentiating their builds.
